import { AbstractControl, ValidationErrors } from '@angular/forms';

export class CustomValidators {
  static blank(control: AbstractControl): ValidationErrors {
    return control.value && control.value.trim() === '' ? { blank: true } : null;
  }

  static onlyDigits(control: AbstractControl): ValidationErrors {
    return control.value && control.value.match(/\D/) ? { onlyDigits: true } : null;
  }
}
