import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { catchError, concatMap, filter, map, takeUntil, tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { CvEntityService } from '../../services';
import { CvEntity, CvEntityView } from '../../models';
import { GeneralInfoComponent } from '../general-info/general-info.component';
import { HistoryComponent } from '../history/history.component';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditorComponent implements OnInit, OnDestroy {

  @ViewChild(GeneralInfoComponent, { static: false })
  private generalInfoComponent: GeneralInfoComponent;

  @ViewChild(HistoryComponent, { static: false })
  private historyComponent: HistoryComponent;

  cvEntity$: Observable<CvEntityView> = of(null);

  private unsubscribe = new Subject();

  get valid(): boolean {
    return this.generalInfoComponent && this.generalInfoComponent.valid
      && this.historyComponent && this.historyComponent.valid;
  }

  get value(): CvEntity {
    return {
      ...this.generalInfoComponent.value,
      history: this.historyComponent.value
    };
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cvEntityService: CvEntityService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.initCvEntity();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  onSaveClick(): void {
    if (!this.valid) {
      return;
    }

    this.cvEntityService.add(this.value).pipe(
      takeUntil(this.unsubscribe),
      tap(entity => this.snackBar.open(`Your CV id is ${ entity.id }`, 'OK'))
    ).subscribe(() => this.router.navigate(['success'], { relativeTo: this.route }));
  }

  private initCvEntity(): void {
    this.cvEntity$ = this.route.queryParams.pipe(
      takeUntil(this.unsubscribe),
      filter(params => params.id),
      map(params => parseInt(params.id, 10)),
      concatMap(id => this.cvEntityService.getOneById(id)),
      catchError((err) => {
        this.snackBar.open(err, 'OK');
        return of(null);
      }),
    );
  }

}
