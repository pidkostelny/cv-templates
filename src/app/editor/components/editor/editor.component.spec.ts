import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { EditorComponent } from './editor.component';
import { CvEntityService } from '../../services';

describe('EditorComponent', () => {
  let component: EditorComponent;
  let fixture: ComponentFixture<EditorComponent>;
  let el: DebugElement;

  beforeEach(waitForAsync(() => {
    const cvEntityServiceSpy = jasmine.createSpyObj('CvEntityService', ['getOneById', 'add']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSnackBarModule,
      ],
      declarations: [
        EditorComponent,
      ],
      providers: [
        { provide: CvEntityService, useValue: cvEntityServiceSpy },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('save button should be disabled by default', () => {
    const saveButton = el.query(By.css('.save-button'));
    expect(saveButton.properties.disabled).toEqual(true);
  });

  it('save button should be enabled if form is valid', () => {
    const saveButton = el.query(By.css('.save-button'));
    expect(saveButton.properties.disabled).toEqual(true);

  });
});
