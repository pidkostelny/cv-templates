import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Technology } from '../../models';
import { CustomValidators } from '../../../shared';

@Component({
  selector: 'app-technologies',
  templateUrl: './technologies.component.html',
  styleUrls: ['./technologies.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TechnologiesComponent implements OnChanges {

  @Input()
  technologies: Technology[];

  technologiesFormArray: FormArray;

  get valid(): boolean {
    return this.technologiesFormArray && this.technologiesFormArray.valid;
  }

  get value(): Technology[] {
    return this.technologiesFormArray.value;
  }

  get addTechnologyEnabled(): boolean {
    return this.technologiesFormArray && this.technologiesFormArray.valid || !this.technologiesFormArray.length;
  }

  constructor(
    private fb: FormBuilder,
  ) {
    this.initForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.technologies && changes.technologies.currentValue) {
      this.updateFormValue();
    }
  }

  onAddClick(): void {
    if (!this.addTechnologyEnabled) {
      return;
    }

    this.technologiesFormArray.push(this.createTechnologyGroup());
  }

  onDeleteClick(index: number): void {
    this.technologiesFormArray.removeAt(index);
  }

  private initForm(): void {
    this.technologiesFormArray = this.fb.array([ this.createTechnologyGroup() ], Validators.required);
  }

  private createTechnologyGroup(technology?: Technology): FormGroup {
    const group = this.fb.group({
      name: ['', [Validators.required, CustomValidators.blank]],
      version: [null],
    });

    if (technology) {
      group.patchValue(technology);
    }
    return group;
  }

  private updateFormValue(): void {
    this.technologiesFormArray.clear();
    this.technologies.forEach(val =>
      this.technologiesFormArray.push( this.createTechnologyGroup(val) )
    );
    if (!this.technologiesFormArray.length) {
      this.technologiesFormArray.push(this.createTechnologyGroup());
    }
  }
}
