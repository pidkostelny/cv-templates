import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Component, DebugElement } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { By } from '@angular/platform-browser';

import { TechnologiesComponent } from './technologies.component';
import { Technology } from '../../models';

@Component({ template: `<app-technologies [technologies]="technologies"></app-technologies>` })
class InputComponent {
  technologies: Technology[];
}

const validTechnologies: Technology[] = [
  { name: 'Angular', version: '8' },
  { name: 'RxJs' },
  { name: 'JS', version: 'ES7' },
];

const invalidTechnologies: Technology[] = [
  { name: '', version: '8' },
  { name: null, version: null },
];

describe('TechnologiesComponent', () => {
  let component: TechnologiesComponent;
  let input: InputComponent;
  let fixture: ComponentFixture<InputComponent>;
  let el: DebugElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ CommonModule, ReactiveFormsModule, NoopAnimationsModule, MatInputModule, MatButtonModule, MatIconModule, ],
      declarations: [
        InputComponent,
        TechnologiesComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputComponent);
    input = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(TechnologiesComponent)).componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create empty technology', () => {
    expect(component.technologiesFormArray.length).toEqual(1);
  });

  it('should update form array when input passed', () => {
    input.technologies = validTechnologies;
    fixture.detectChanges();

    expect(component.value.length)
      .toEqual(validTechnologies.length);
  });

  it('should be valid when valid data passed', () => {
    input.technologies = validTechnologies;
    fixture.detectChanges();

    expect(component.valid).toBeTrue();
  });

  it('should be invalid when invalid data passed', () => {
    input.technologies = invalidTechnologies;
    fixture.detectChanges();

    expect(component.valid).toBeFalse();
  });

  it('should return the same value if no changes', () => {
    input.technologies = invalidTechnologies;
    fixture.detectChanges();

    expect(component.value).toEqual(invalidTechnologies);
  });

  it('should add technology button be enabled for valid data', () => {
    input.technologies = validTechnologies;
    fixture.detectChanges();

    const addButton = el.query(By.css('.add-button'));
    expect(addButton.properties.disabled).toBeFalse();
  });

  it('should add technology button be disabled by default and for invalid data', () => {
    const addButton = el.query(By.css('.add-button'));
    expect(addButton.properties.disabled).toBeTrue();

    input.technologies = invalidTechnologies;
    fixture.detectChanges();

    expect(addButton.properties.disabled).toBeTrue();
  });

  it('should add button create an empty item if form valid', () => {
    const addButton = el.query(By.css('.add-button'));

    input.technologies = validTechnologies;
    fixture.detectChanges();

    expect(addButton.properties.disabled).toBeFalse();
    addButton.triggerEventHandler('click', null);
    expect(component.value.length).toBeGreaterThan(validTechnologies.length);
  });

  it('should add button do nothing if invalid', () => {
    const addButton = el.query(By.css('.add-button'));
    expect(component.value.length).toBe(1);

    expect(addButton.properties.disabled).toBeTrue();
    addButton.triggerEventHandler('click', null);
    expect(component.value.length).toBe(1);
  });
});

