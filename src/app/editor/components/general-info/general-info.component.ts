import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { CustomValidators } from '../../../shared';
import { GeneralInfo } from '../../models';

@Component({
  selector: 'app-general-info',
  templateUrl: './general-info.component.html',
  styleUrls: ['./general-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GeneralInfoComponent implements OnChanges {
  @Input()
  generalInfo: GeneralInfo;

  generalInfoForm: FormGroup;
  readonly nameControl = new FormControl('', [Validators.required, CustomValidators.blank]);
  readonly emailControl = new FormControl('', [Validators.required, Validators.email]);
  readonly phoneControl = new FormControl('', [Validators.required, CustomValidators.onlyDigits]);

  get valid(): boolean {
    return this.generalInfoForm && this.generalInfoForm.valid;
  }

  get value(): GeneralInfo {
    return {
      id: this.generalInfo && this.generalInfo.id,
      ...this.generalInfoForm.value,
    };
  }

  constructor(
    private fb: FormBuilder,
  ) {
    this.initForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.generalInfo && changes.generalInfo.currentValue) {
      this.updateFormValueWithTick();
    }
  }

  private initForm(): void {
    this.generalInfoForm = this.fb.group({
      name: this.nameControl,
      email: this.emailControl,
      phone: this.phoneControl,
    });
  }

  private updateFormValueWithTick(): void {
    setTimeout(() => this.generalInfoForm.patchValue(this.generalInfo || {}), 0);
  }
}
