import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  QueryList,
  SimpleChanges,
  ViewChildren
} from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { HistoryItem } from '../../models';
import { CustomValidators } from '../../../shared';
import { TechnologiesComponent } from '../technologies/technologies.component';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HistoryComponent implements OnChanges {

  @Input()
  historyItems: HistoryItem[];

  @ViewChildren(TechnologiesComponent)
  technologiesLists: QueryList<TechnologiesComponent>;

  historyFormArray: FormArray;
  readonly today = new Date();
  readonly futureDatesFilter = (d: Date) => this.today > (d || new Date());

  get valid(): boolean {
    this.cdr.markForCheck();
    return this.historyFormArray && this.historyFormArray.valid && this.technologiesValid;
  }

  get value(): any {
    const technologies = this.technologiesLists.toArray();
    return this.historyFormArray.value.map((val, i) => ({
      ...val,
      technologies: technologies[i].value
    }));
  }

  get addHistoryItemEnabled(): boolean {
    return this.valid || !this.historyFormArray.length;
  }

  private get technologiesValid(): boolean {
    return this.technologiesLists && this.technologiesLists.reduce<boolean>(
      (res, component) => component.valid && res,
      true
    );
  }

  constructor(
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
  ) {
    this.initForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.historyItems && changes.historyItems.currentValue) {
      this.updateFormValue();
    }
  }

  onAddClick(): void {
    if (!this.addHistoryItemEnabled){
      return;
    }

    this.historyFormArray.push(this.createHistoryGroup());
  }

  onDeleteClick(index: number): void {
    this.historyFormArray.removeAt(index);
    this.cdr.detectChanges();
  }

  getErrorForDatepickerByFormControl(control: AbstractControl): string {
    if (!control) {
      return '';
    }

    if (control.hasError('matDatepickerParse')) {
      return 'Not valid';
    }

    if (control.hasError('required')) {
      return 'Required';
    }
  }

  private initForm(): void {
    this.historyFormArray = this.fb.array([ this.createHistoryGroup() ], Validators.required);
  }

  private createHistoryGroup(history?: HistoryItem): FormGroup {
    const group = this.fb.group({
      company: ['', [Validators.required, CustomValidators.blank]],
      role: ['', [Validators.required, CustomValidators.blank]],
      period: this.fb.group({
        from: [null, [Validators.required]],
        to: [null],
      }),
      summary: ['', [Validators.minLength(100), Validators.maxLength(250), CustomValidators.blank]]
    });

    if (history) {
      group.patchValue(history);
    }
    return group;
  }

  private updateFormValue(): void {
    this.historyFormArray.clear();
    this.historyItems.forEach(val =>
      this.historyFormArray.push( this.createHistoryGroup(val) )
    );

    if (!this.historyFormArray.length) {
      this.historyFormArray.push(this.createHistoryGroup());
    }
  }
}
