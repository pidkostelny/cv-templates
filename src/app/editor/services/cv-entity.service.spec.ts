import { TestBed } from '@angular/core/testing';

import { CvEntityService } from './cv-entity.service';

describe('CvEntityService', () => {
  let service: CvEntityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ CvEntityService ]
    });
    service = TestBed.inject(CvEntityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
