import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { concatMap, filter, map, mergeAll, tap, throwIfEmpty } from 'rxjs/operators';

import { CvEntity, CvEntityView } from '../models';

const initialID = 1;

@Injectable()
export class CvEntityService {
  static readonly storageKey = 'cvTemplates-v1';

  private get cvEntities(): CvEntity[] {
    return JSON.parse(localStorage.getItem(CvEntityService.storageKey));
  }

  constructor() {
    if (!localStorage.getItem(CvEntityService.storageKey)) {
      localStorage.setItem(CvEntityService.storageKey, JSON.stringify([]));
    }
  }

  get(): Observable<CvEntity[]> {
    return of<CvEntity[]>(this.cvEntities);
  }

  getOneById(id: number): Observable<CvEntityView> {
    return this.get().pipe(
      mergeAll(),
      filter(cvEntity => cvEntity.id === id),
      throwIfEmpty(() => new Error(`No record found for id ${ id }`)),
      map(cvEntity => new CvEntityView(cvEntity))
    );
  }

  add(cvEntity: CvEntity): Observable<CvEntity> {
    let save$: Observable<CvEntity[]> = this.get();
    if (cvEntity.id) {
      save$ = save$.pipe(
        tap(items => items[items.findIndex(el => el.id === cvEntity.id)] = cvEntity),
      );
    } else {
      save$ = save$.pipe(
        tap(cvEntities => cvEntity.id = cvEntities.length ? cvEntities[cvEntities.length - 1].id + 1 : initialID),
        tap(cvEntities => cvEntities.push(cvEntity)),
      );
    }

    return save$.pipe(
      tap(this.saveCvEntities),
      concatMap(() => of(cvEntity))
    );
  }

  private saveCvEntities(cvEntities: CvEntity[]): void {
    localStorage.setItem(CvEntityService.storageKey, JSON.stringify(cvEntities));
  }
}
