import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

import { EditorRoutingModule } from './editor-routing.module';
import { CvEntityService } from './services';
import { EditorComponent } from './components/editor/editor.component';
import { GeneralInfoComponent } from './components/general-info/general-info.component';
import { HistoryComponent } from './components/history/history.component';
import { TechnologiesComponent } from './components/technologies/technologies.component';
import { SuccessComponent } from './components/success/success.component';


@NgModule({
  declarations: [
    EditorComponent,
    GeneralInfoComponent,
    HistoryComponent,
    TechnologiesComponent,
    SuccessComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EditorRoutingModule,
    MatCardModule,
    MatInputModule,
    MatGridListModule,
    MatButtonModule,
    MatSnackBarModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  providers: [
    CvEntityService,
  ],
})
export class EditorModule { }
