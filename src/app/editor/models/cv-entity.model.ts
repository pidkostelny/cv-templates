import { HistoryItem } from './history-item.model';


export interface CvEntity {
  id?: number;
  name: string;
  email: string;
  phone: string;
  history: Array<HistoryItem>;
}
