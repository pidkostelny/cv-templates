export interface Technology {
  name: string;
  version?: string;
}
