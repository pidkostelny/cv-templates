import { Technology } from './technology.model';

export interface HistoryItem {
  company: string;
  role: string;
  period: {
    from: Date;
    to?: Date;
  };
  summary: string;
  technologies: Array<Technology>;
}
