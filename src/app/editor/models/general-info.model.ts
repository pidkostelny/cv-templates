
export interface GeneralInfo {
  id?: number;
  name: string;
  email: string;
  phone: string;
}
