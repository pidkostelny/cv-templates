import { GeneralInfo } from './general-info.model';
import { HistoryItem } from './history-item.model';
import { CvEntity } from './cv-entity.model';

export class CvEntityView {
  generalInfo: GeneralInfo;
  history: Array<HistoryItem>;

  constructor(cvEntity: CvEntity) {
    const { name, email, phone, history, id } = cvEntity;
    this.generalInfo = { name, email, phone, id };
    this.history = history;
  }
}
